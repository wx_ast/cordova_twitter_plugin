/*
 Twitter.m
 */

#import <objc/runtime.h>
#import "CDVTwitter.h"
#import "STTwitter.h"


// For older versions of Cordova, you may have to use: #import "CDVDebug.h"
#import <Cordova/CDVDebug.h>

@implementation CDVTwitter

- (void)login:(CDVInvokedUrlCommand*)command
{
    STTwitterAPI *twitter = [STTwitterAPI twitterAPIWithOAuthConsumerName:nil
        consumerKey:@"5C1ohd6sddVKiEW3bObRA"
        consumerSecret:@"SNHnbjltIiLVDWCe0FAmKd22ZPCXTai5ihtAgmNyZpk"];

    [twitter postReverseOAuthTokenRequest:^(NSString *authenticationHeader) {
        STTwitterAPI *twitterAPIOS = [STTwitterAPI twitterAPIOSWithFirstAccount];
        [twitterAPIOS verifyCredentialsWithSuccessBlock:^(NSString *username) {
            [twitterAPIOS postReverseAuthAccessTokenWithAuthenticationHeader:authenticationHeader
                successBlock:^(NSString *oAuthToken,
                NSString *oAuthTokenSecret,
                NSString *userID,
                NSString *screenName) {
                    [self success:userID token:oAuthToken];
                } errorBlock:^(NSError *error) {
                    [self fail:error];
                    NSLog(@"error 1: %@", [error localizedDescription]);
                }];
        } errorBlock:^(NSError *error) {
            NSLog(@"error 2: %@", [error localizedDescription]);
            [self fail:error];
            // if (error.code == 6) {
            //     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
            //         message:@"Нужно завести хотя бы один аккаунт в настройках"
            //         delegate:nil
            //         cancelButtonTitle:@"OK"
            //         otherButtonTitles:nil];
            //     [alert show];
            // }
        }];

    } errorBlock:^(NSError *error) {
        [self fail:error];
        NSLog(@"error 3: %@", [error localizedDescription]);
    }];
}

- (void)success:(int)user_id token:(NSString*)token
{
    NSLog(@"+success");
    NSString * jsCallBack =  [NSString stringWithFormat:@"cordova.require('cordova/plugin/Twitter').execDone(true, {user_id: '%d', token: '%@'});", user_id, token];
    [self writeJavascript:jsCallBack];
}

- (void)fail:(NSError*)error
{
    NSLog(@"+fail");
    NSString * jsCallBack = [NSString stringWithFormat:@"cordova.require('cordova/plugin/Twitter').execDone(false, {error: '%@', code: %d});", [error localizedDescription], error.code];
    [self writeJavascript:jsCallBack];
}

@end
