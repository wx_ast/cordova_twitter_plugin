/*
 Twitter.h
 */
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// For older versions of Cordova, you may have to use: #import "CDVPlugin.h"
#import <Cordova/CDVPlugin.h>

@interface CDVTwitter : CDVPlugin

- (void)login:(CDVInvokedUrlCommand*)command;

- (void)success:(NSError*)error;
- (void)fail:(int)user_id token:(NSString*)token;


@end
